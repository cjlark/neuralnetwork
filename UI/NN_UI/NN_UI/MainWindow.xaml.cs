﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NN_UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Process myProcess, NDIProcess, optoProcess;
        //Socket Variables
        Socket connectedSocket = null;
        byte[] bytes = new byte[10];
        //Timer Variables
        int seconds = 0;
        int minutes = 0;
        //Connection Flags
        bool timerFlag = false;
        bool opto = false;
        bool ndi = false;
        bool kuka = false;
        //Color Variables
        SolidColorBrush red = new SolidColorBrush();
        SolidColorBrush white = new SolidColorBrush();
        SolidColorBrush green = new SolidColorBrush();
        //Timers
        System.Windows.Threading.DispatcherTimer serialComsTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer statusUpdateTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer timeDisplay = new System.Windows.Threading.DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();

            //initialize colors of the status bars
            red.Color = Color.FromRgb(255, 0, 0);
            white.Color = Color.FromRgb(255, 255, 255);
            green.Color = Color.FromRgb(0, 255, 0);
            optoStatus.Fill = red;
            ndiStatus.Fill = red;
            kukaStatus.Fill = red;

            serialComsTimer.Tick += serialComsTimer_Tick;
            serialComsTimer.Interval = new TimeSpan(0, 0, 0, 0, 1); //updates every 10 ms

            statusUpdateTimer.Tick += statusUpdateTimer_Tick;
            statusUpdateTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); //updates every 500 ms
            statusUpdateTimer.Start();

            timeDisplay.Tick += timeDisplay_Tick;
            timeDisplay.Interval = new TimeSpan(0, 0, 0, 1, 0); //updates every 1s
            timeDisplay.Start();
        }

        private void serialComsTimer_Tick(object sender, EventArgs e) //checks for updated messages
        {
            
            if (connectedSocket != null && connectedSocket.Connected)
            {
                //need to do something here if the socket isnt sending anyting!!
                if (connectedSocket.Available != 0)
                {
                    int i = connectedSocket.Receive(bytes);
                   //tstTxt.Text = i.ToString();
                    string inMessage = Encoding.UTF8.GetString(bytes);
                    incomingMessage.Text = inMessage;
                    if (inMessage[0] == '1')
                    {
                        opto = true;
                    }
                    else
                    {
                        opto = false;
                    }
                    if (inMessage[2] == '1')
                    {
                        ndi = true;
                    }
                    else
                    {
                        ndi = false;
                    }
                    if (inMessage[4] == '1')
                    {
                        kuka = true;
                    }
                    else
                    {
                        kuka = false;
                    }
                }

            }
            else
            {
                kuka = opto = ndi = false;
            }
        }

        private void statusUpdateTimer_Tick(object sender, EventArgs e)
        {
    
            if (opto == false) //if optoTrak socket is not connected
            {
                if(optoStatus.Fill == red) // if the color is red change to white
                {
                    optoStatus.Fill = white;
                }
                else // its white
                {
                    optoStatus.Fill = red;
                }
            }
            else
            {
                optoStatus.Fill = green;
            }

            if (ndi == false) //if ndi socket is not connected
            {
                if (ndiStatus.Fill == red) // if the color is red change to white
                {
                    ndiStatus.Fill = white;
                }
                else // its white
                {
                    ndiStatus.Fill = red;
                }
            }
            else
            {
                ndiStatus.Fill = green;
            }

            if (kuka == false) //if kuka socket is not connected
            {
                if (kukaStatus.Fill == red) // if the color is red change to white
                {
                    kukaStatus.Fill = white;
                }
                else // its white
                {
                    kukaStatus.Fill = red;
                }
            }
            else
            {
                kukaStatus.Fill = green;
            }
        }

        private void timeDisplay_Tick(object sender, EventArgs e)
        {
            if(opto && kuka && ndi) //if all sockets are connected and streaming start timer
            {
                timerFlag = true;
            }
            if (timerFlag)
            {
                if (seconds == 60 && seconds != 0) //if seconds are at 60 increment minute and reset seconds to zero
                {
                    minutes++;
                    seconds = 0;
                }
                seconds++;
                timeTxt.Text = minutes.ToString() + " mins " + seconds.ToString() + " s";
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (startButton.Content.ToString() == "Start Connection")
            {
                myProcess = Process.Start("C:\\Users\\cjl\\Desktop\\NN Repo\\build\\executables\\Release\\streamManager.exe");

                startButton.Content = "Connecting...";
                connectedSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 2001);

                connectedSocket.Connect(remoteEP);
                serialComsTimer.Start();

                if (filePath.Text == "Enter File Path") //send the file path information
                {
                    UIMessage.Text = "Did not select a file path!";
                }
                else
                {
                    string sendMessage = "";
                    sendMessage += filePath.Text;
                    sendMessage += ",";
                    sendMessage += numSamples.Text;
                    sendMessage += ",";
                    sendMessage += sampleRate.Text;
                    sendMessage += ",";
                    if(Collection_Type.SelectedIndex == 1) // If the collection method is set to Live Data
                    {
                        //optoProcess = Process.Start("C:\\Users\\cjl\\Desktop\\Optotrak Certus\\build\\Executables\\Release\\Run_Opto.exe");
                        Thread.Sleep(5000);
                        NDIProcess = Process.Start("C:\\Users\\cjl\\Desktop\\NDI Stream\\SimpleSerial\\Build\\executables\\Release\\simpleSerialMain.exe");

                        sendMessage += "live;";
                    } else if(Collection_Type.SelectedIndex == 2)
                    {
                        optoProcess = Process.Start("C:\\Users\\cew\\Desktop\\Optotrak Certus\\build\\Executables\\Release\\Run_Opto.exe");
                        Thread.Sleep(5000);
                        NDIProcess = Process.Start("C:\\Users\\cew\\Desktop\\NDI Stream\\SimpleSerial\\Build\\executables\\Release\\simpleSerialMain.exe");

                        sendMessage += "vs;";
                    }
                    else // Default to test collection
                    {
                        
                       NDIProcess = Process.Start("C:\\Users\\cjl\\Desktop\\NDI Stream\\SimpleSerial\\Build\\executables\\Release\\simpleSerialMain.exe");

                        sendMessage += "test;";
                    }
                    connectedSocket.Send(Encoding.ASCII.GetBytes(sendMessage));
                }
            }
            //put code here that sends feature data and file path
        }

        private void restartButton_Click(object sender, RoutedEventArgs e)
        {
            //myProcess.Kill(); //closes executable
            opto = false;
            kuka = false;
            ndi = false;
           
            startButton.Content = "Start Connection";
        }
    }
}
