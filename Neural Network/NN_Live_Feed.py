from __future__ import print_function

import threading
from queue import Queue
import math

import socket, pickle
from IPython import display
from itertools import *
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.contrib.predictor import predictor
from tensorflow.contrib.predictor import from_estimator
from tensorflow.estimator import Estimator
import time
import multiprocessing
from multiprocessing.pool import Pool
from tensorflow.python.data import Dataset

tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format

nnDataHolder = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\testing7.csv", sep=",")# Filling an object with training data
nnDataHolder = nnDataHolder.reindex(np.random.permutation(nnDataHolder.index))                    # Randomizes the data

def predict_value(optoPred, queue):
    Pred = ""
    Pred = list(p["predictions"] for p in islice(optoPred, 1))[0]
    print(Pred)
    queue.put(Pred)

def sort_outgoing_data(data):
    #pickle unpackages the DataFrame so that it can be recieved
    string = ",".join(map(str,data))
    return(string)

def sort_incoming_data(d):
    #data comes in as bytes- converted to string
    d = d.decode()
    d = d.split(",")
    df = pd.DataFrame({'NDI_X':[float(d[0])], 'NDI_Y':[float(d[1])], 'NDI_Z':[float(d[2])], 'NDI_Rot_X1':[float(d[3])], 'NDI_Rot_X2':[float(d[4])], 'NDI_Rot_X3':[float(d[5])], 'NDI_Rot_Y1':[float(d[6])], 'NDI_Rot_Y2':[float(d[7])],
                       'NDI_Rot_Y3': [float(d[8])],'NDI_Rot_Z1':[float(d[9])],'NDI_Rot_Z2':[float(d[10])],'NDI_Rot_Z3':[float(d[11])],'NDI_TSL':[float(d[12])],'NDI_Quality':[float(d[13])],'Kuka_X':[float(d[14])],'Kuka_Y':[float(d[15])],
                       'Kuka_Z': [float(d[16])],'Kuka_A':[float(d[17])],'Kuka_B':[float(d[18])],'Kuka_C':[float(d[19])],'Kuka_TSL':[float(d[20])]})
    return (df)



def preprocess_features(nnDataHolder):
    """Prepares input features from the nnDataHolder dataset.

    Args:
        nnDataHolder: A Pandas DataFrame which contains data from test dataset.
    Returns:
        A DataFrame that contains the features to be used for the model (including synthetic features)
    """

    selected_features = nnDataHolder[
        ["NDI_X",
         "NDI_Y",
         "NDI_Z",
         "NDI_Rot_X1",
         "NDI_Rot_X2",
         "NDI_Rot_X3",
         "NDI_Rot_Y1",
         "NDI_Rot_Y2",
         "NDI_Rot_Y3",
         "NDI_Rot_Z1",
         "NDI_Rot_Z2",
         "NDI_Rot_Z3",
         "NDI_Quality",
         "NDI_TSL",
         "Kuka_X",
         "Kuka_Y",
         "Kuka_Z",
         "Kuka_A",
         "Kuka_B",
         "Kuka_C",
         "Kuka_TSL"]]
    processed_features = selected_features.copy()
    # Create any needed synthetic features here.
    return processed_features

def construct_feature_column(input_features):
    """Construct the TensorFlow Feature Columns.
    Args:
        input_features: the names of the numerical input features to use during taining.
    Returns:
        A set of feature columns
    """
    return set([tf.feature_column.numeric_column(my_feature) for my_feature in input_features])

def preprocess_targets(nnDataHolder, optoTargetLabel):
    """Prepares target features (ie Labels/Answers).

    Args:
        nnDataHolder : Pandas DataFrame expected to contain testing data
    Returns:
        output_targets : Pandas DataFrame which contains the labels from the data
    """

    output_targets = pd.DataFrame()

    # Set the target to the desired label
    output_targets[optoTargetLabel] = (nnDataHolder[optoTargetLabel])

    return output_targets

def loadModel(checkpointName, hidden_units, learning_rate, features):
    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
    dnn_regressor = tf.estimator.DNNRegressor(
        feature_columns=construct_feature_column(features),
        hidden_units=hidden_units,
        optimizer=my_optimizer,
        model_dir=checkpointName
    )
    return dnn_regressor

def my_input_function(features, batch_size=1, shuffle = True, num_epochs = None):
    """This function is used to train a neural network regression model

    Args:
        features: pandas DataFrame of features
        targets: pandas DataFrame of targets
        batch_size: size of batches to be passed to the model (# of examples used in a batch)
        batch: the set of examples used in one iteration of training
        shuffle: True or False. Whether or not to shuffle the data
        num_epochs: number of epochs for which the data should be repeated. None = repeat indefinitely
    Returns:
        Tuple of (features, labels) for next data batch
    """

    # Convert Pandas data into a dict of np arrays because this is what dataset takes
    # We used a Pandas DataFrame to initially store the data and now are converting it to a dictionary -> {col1: [row1,row2,...], col2:[row1,row2..],...}
    features = {key:np.array(value) for key,value in dict(features).items()}
    print(features)
    # Construct a dataset, and configure the batching/repeating
    ds = Dataset.from_tensor_slices((features))
    ds = ds.batch(batch_size).repeat(num_epochs)
    # Return the next batch of data
    features = ds.make_one_shot_iterator().get_next()
    return features



def main():
    # Declare a socket and try to connect on port 2005
    prediction_examples = preprocess_features(nnDataHolder.head(1))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 2005
    try:
        s.bind((host, port))
    except socket.error:
        print("Binding Failed")
    s.listen(4)
    print("")
    print("Listening for client . . .")
    conn, address = s.accept()
    print("Connected to client at ", address)
    print("")

    optoPredX = loadModel(learning_rate=0.001,
                         checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoX2.ckpt',
                         hidden_units=[10, 10, 10, 10], features=prediction_examples)
    optoPredY = loadModel(learning_rate=0.001,
                         checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoY2.ckpt',
                         hidden_units=[10, 10, 10, 10], features=prediction_examples)
    optoPredZ = loadModel(learning_rate=0.001,
                         checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoZ2.ckpt',
                         hidden_units=[10, 10, 10, 10], features=prediction_examples)
    optoPredW = loadModel(learning_rate=0.001,
                         checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoW2.ckpt',
                         hidden_units=[30, 30, 30, 10], features=prediction_examples)
    optoPredQx = loadModel(learning_rate=0.001,
                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQx2.ckpt',
                          hidden_units=[10, 10, 10, 10], features=prediction_examples)
    optoPredQy = loadModel(learning_rate=0.001,
                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQy1.ckpt',
                          hidden_units=[10, 10, 10, 10], features=prediction_examples)
    optoPredQz = loadModel(learning_rate=0.001,
                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQz1.ckpt',
                          hidden_units=[10, 10, 10, 10], features=prediction_examples)
    # Loop where data is gathered
    while True:
        output = conn.recv(2048)
        if output: # If there is data to be received
            print("GOT HERE")
            incomingData = sort_incoming_data(output)
            print(incomingData)

            #prediction_examples = preprocess_features(incomingData)
            #current_predictionX = optoPredX.predict(input_fn=lambda: my_input_function(incomingData))
            xEst = Estimator.__init__(model_dir='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoX2.ckpt')
            current_predictionX = xEst.predict(estimator=optoPredX, prediction_input_fn=lambda: my_input_function(incomingData))

            current_predictionY = optoPredY.predict(input_fn=lambda: my_input_function(incomingData))
            current_predictionZ = optoPredZ.predict(input_fn=lambda: my_input_function(incomingData))
            current_predictionW = optoPredW.predict(input_fn=lambda: my_input_function(incomingData))
            current_predictionQx = optoPredQx.predict(input_fn=lambda: my_input_function(incomingData))
            current_predictionQy = optoPredQy.predict(input_fn=lambda: my_input_function(incomingData))
            current_predictionQz = optoPredQz.predict(input_fn=lambda: my_input_function(incomingData))
            print("GOT HERE2")

            queueX = Queue()
            queueY = Queue()
            queueZ = Queue()
            queueW = Queue()
            queueQx = Queue()
            queueQy = Queue()
            queueQz = Queue()

            threadX = threading.Thread(target=predict_value, args=(current_predictionX, queueX))
            threadY = threading.Thread(target=predict_value, args = (current_predictionY,queueY))
            threadZ = threading.Thread(target=predict_value, args = (current_predictionZ,queueZ))
            threadW = threading.Thread(target=predict_value, args = (current_predictionW,queueW))
            threadQx = threading.Thread(target=predict_value, args = (current_predictionQx,queueQx))
            threadQy = threading.Thread(target=predict_value, args = (current_predictionQy,queueQy))
            threadQz = threading.Thread(target=predict_value, args = (current_predictionQz,queueQz))

            threadX.start()
            threadY.start()
            threadZ.start()
            threadW.start()
            threadQx.start()
            threadQy.start()
            threadQz.start()
            threadX.join()
            threadY.join()
            threadZ.join()
            threadW.join()
            threadQz.join()
            threadQy.join()
            threadQz.join()
            print("GOT HERE3")
            OptoXPred = queueX.get()
            OptoYPred = queueY.get()
            OptoZPred = queueZ.get()
            OptoWPred = queueW.get()
            OptoQxPred = queueQx.get()
            OptoQyPred = queueQy.get()
            OptoQzPred = queueQz.get()

            print("GOT HERE4")
            print("GOT HERE5")
            outgoingData =""
            outgoingData = outgoingData + sort_outgoing_data(OptoXPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoYPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoZPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoWPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoQxPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoQyPred)
            outgoingData = outgoingData + ","
            outgoingData = outgoingData + sort_outgoing_data(OptoQzPred)
            outgoingData = outgoingData.encode()
            conn.send(outgoingData)
main()