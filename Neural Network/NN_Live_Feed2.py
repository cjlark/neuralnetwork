from __future__ import print_function

import threading
from queue import Queue
import math

import socket, pickle
from IPython import display
from itertools import *
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.contrib.predictor import predictor
from tensorflow.contrib.predictor import from_estimator
from tensorflow.estimator import Estimator
import time
import multiprocessing
from multiprocessing.pool import Pool
from tensorflow.python.data import Dataset

FEATURE_DIM = 19
LABEL_DIM = 1
tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format

#nnDataHolder = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\testing7.csv", sep=",")# Filling an object with training data
#nnDataHolder = nnDataHolder.reindex(np.random.permutation(nnDataHolder.index))                    # Randomizes the data

def predict_value(optoPred, queue):
    Pred = ""
    Pred = list(p["predictions"] for p in islice(optoPred, 1))[0]
    queue.put(Pred)

# Data comes in as a dict of predicted values
def sort_outgoing_data(data):

    preds = ','.join(str(v) for v in np.nditer(data["output"]))
    print(preds)
    return preds



def sort_incoming_data(d):
    #data comes in as bytes- converted to string
    d = d.decode()
    d = d.split(",")
    df = pd.DataFrame({'NDI_X':[float(d[0])], 'NDI_Y':[float(d[1])], 'NDI_Z':[float(d[2])], 'NDI_Rot_X1':[float(d[3])], 'NDI_Rot_X2':[float(d[4])], 'NDI_Rot_X3':[float(d[5])], 'NDI_Rot_Y1':[float(d[6])], 'NDI_Rot_Y2':[float(d[7])],
                       'NDI_Rot_Y3': [float(d[8])],'NDI_Rot_Z1':[float(d[9])],'NDI_Rot_Z2':[float(d[10])],'NDI_Rot_Z3':[float(d[11])],'NDI_Quality':[float(d[12])],'Kuka_X':[float(d[13])],'Kuka_Y':[float(d[14])],
                       'Kuka_Z': [float(d[15])],'Kuka_A':[float(d[16])],'Kuka_B':[float(d[17])],'Kuka_C':[float(d[18])]})
    inputnums = df.values
    inputnums = np.float32(inputnums)
    inputnums = inputnums.reshape(1, FEATURE_DIM)
    inputs = {"x": inputnums}
    return (inputs)


def serving_input_fn():
    x = tf.placeholder(dtype=tf.float32, shape=[None,FEATURE_DIM], name ='x')
    inputs = {'x':x}
    return tf.estimator.export.ServingInputReceiver(inputs,inputs)

def model_fn(features, labels, mode):
    nn = features["x"]
    for hiddenUnits in [10, 10]:
        nn = tf.layers.dense(nn, units=hiddenUnits, activation=tf.nn.leaky_relu)  # set up activation fn
        nn = tf.layers.dropout(nn,
                               rate=0.0)  # dropout randomly settings the fraction of input units to drop to avoid overfitting

    output = tf.layers.dense(nn, LABEL_DIM, activation=None)

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=output,
                                          export_outputs={"out": tf.estimator.export.PredictOutput(output)})

    loss = tf.losses.mean_squared_error(labels, output)

    if mode == tf.estimator.ModeKeys.EVAL:  # sets EVAL to output the mean square error
        return tf.estimator.EstimatorSpec(mode, loss=loss)

    # Define optimizer as Momentum optimier (CAN CHANGE LATER)
    # optimizer = tf.train.AdagradOptimizer(learning_rate=0.00001)
    # optimizer = tf.train.AdadeltaOptimizer(learning_rate=0.0001)
    optimizer = tf.train.MomentumOptimizer(learning_rate=0.000001, momentum=0.9)
    # optimizer = tf.train.AdamOptimizer(learning_rate=0.000001)
    train = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train)



if __name__ == "__main__":
    # Declare a socket and try to connect on port 2005
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 2005
    try:
        s.bind((host, port))
    except socket.error:
        print("Binding Failed")
    s.listen(4)
    print("")
    print("Listening for client . . .")
    conn, address = s.accept()
    print("Connected to client at ", address)
    """
    print("")
    print("Creating Predictor")
    estimator = tf.estimator.Estimator(model_fn=model_fn,
                                       model_dir="C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Gen2Save\\estimator-predictor-test-2018-09-14 14.38.34")

    predictor = from_estimator(estimator, serving_input_receiver_fn=serving_input_fn)
    print("Predictor created.")
    i = 0
    print("Predicting")
    while True:
        #output = conn.recv(2048)
        #if output: # If there is data to be received
            #incomingData = sort_incoming_data(output)
        inputnums = np.array([258.961000,-25.784000,-36.388000,-0.051000,-0.020000,0.998000,-0.463000,0.886000,-0.006000,-0.885000,-0.463000,-0.054000,904.000000,534.905000,-25.547000,305.833000,3.116000,0.040000,3.141000])
        inputnums = inputnums.reshape(1, FEATURE_DIM)
        inputs = {"x": inputnums}
        prediction = predictor(inputs)

          #outgoingData = sort_outgoing_data(prediction)
          #outgoingData = outgoingData.encode()
          #conn.send(outgoingData)
        print(prediction)
        print("hi")
        """
        inputnums = np.array([])
        inputnums = inputnums.reshape(1, FEATURE_DIM)
        inputs = {"x": inputnums}
        prediction = predictor(inputs)

        print(prediction)
        """
