from __future__ import print_function

import math
import shutil
from IPython import display
from itertools import islice
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.3f}'.format


nnDataHolder = pd.read_csv("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\testingLin3.csv", sep=",")# Filling an object with training data
nnDataHolder = nnDataHolder.reindex(np.random.permutation(nnDataHolder.index))                    # Randomizes the data

def preprocess_features(nnDataHolder):
    """Prepares input features from the nnDataHolder dataset.

    Args:
        nnDataHolder: A Pandas DataFrame which contains data from test dataset.
    Returns:
        A DataFrame that contains the features to be used for the model (including synthetic features)
    """

    selected_features = nnDataHolder[
        ["NDI_X",
         "NDI_Y",
         "NDI_Z",
         "NDI_Rot_X1",
         "NDI_Rot_X2",
         "NDI_Rot_X3",
         "NDI_Rot_Y1",
         "NDI_Rot_Y2",
         "NDI_Rot_Y3",
         "NDI_Rot_Z1",
         "NDI_Rot_Z2",
         "NDI_Rot_Z3",
         "NDI_Quality",
         "NDI_TSL",
         "Kuka_X",
         "Kuka_Y",
         "Kuka_Z",
         "Kuka_A",
         "Kuka_B",
         "Kuka_C",
         "Kuka_TSL"]]
    processed_features = selected_features.copy()
    # Create any needed synthetic features here.
    return processed_features


def preprocess_targets(nnDataHolder, optoTargetLabel):
    """Prepares target features (ie Labels/Answers).

    Args:
        nnDataHolder : Pandas DataFrame expected to contain testing data
    Returns:
        output_targets : Pandas DataFrame which contains the labels from the data
    """

    output_targets = pd.DataFrame()

    # Set the target to the desired label
    output_targets[optoTargetLabel] = (nnDataHolder[optoTargetLabel])

    return output_targets

# Choose 70 percent of the examples to be for training the data
training_examples = preprocess_features(nnDataHolder.head(40000))

#choose 30 percent to be for validation
validation_examples = preprocess_features(nnDataHolder.tail(10000))


prediction_examples = preprocess_features(nnDataHolder.head(1))
prediction_targets = preprocess_targets(nnDataHolder.head(1), "Opto_X")
# Have one input for a test at the end
print("Training examples summary:")
display.display(training_examples.describe())


print("Training targets summary:")
display.display(training_examples.describe())

def construct_feature_column(input_features):
    """Construct the TensorFlow Feature Columns.
    Args:
        input_features: the names of the numerical input features to use during taining.
    Returns:
        A set of feature columns
    """
    return set([tf.feature_column.numeric_column(my_feature) for my_feature in input_features])

def my_input_function(features, targets, batch_size=1, shuffle = True, num_epochs = None):
    """This function is used to train a neural network regression model

    Args:
        features: pandas DataFrame of features
        targets: pandas DataFrame of targets
        batch_size: size of batches to be passed to the model (# of examples used in a batch)
        batch: the set of examples used in one iteration of training
        shuffle: True or False. Whether or not to shuffle the data
        num_epochs: number of epochs for which the data should be repeated. None = repeat indefinitely
    Returns:
        Tuple of (features, labels) for next data batch
    """

    # Convert Pandas data into a dict of np arrays because this is what dataset takes
    # We used a Pandas DataFrame to initially store the data and now are converting it to a dictionary -> {col1: [row1,row2,...], col2:[row1,row2..],...}
    features = {key:np.array(value) for key,value in dict(features).items()}

    # Construct a dataset, and configure the batching/repeating
    ds = Dataset.from_tensor_slices((features,targets))
    ds = ds.batch(batch_size).repeat(num_epochs)

    # Shuffle the data, if specified.
    if shuffle:
        ds = ds.shuffle(10000)

    # Return the next batch of data
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

def train_nn_regression_model(
        learning_rate,
        steps,
        batch_size,
        targetLabel,
        hidden_units,
        training_examples,
        training_targets,
        validation_examples,
        validation_targets,
        checkpointName):
    """Trains a neural network regression model.

    In addition to training, this function also prints training progress info and
    a plot of the training validation loss over time.

    Args:
        learning_rate: A 'float' that represents how large of steps the iterative approach should take
        steps: A non-zero 'int', the total number training steps. A stingle step consists of a forward and backward
               pass using a single batch
        batch_size: A non-zero 'int' that represents the batch size
        hidden_units: A 'list' of int values, specifying the number of neurons in each layer
        training_examples: A 'DataFrame' containing one or more columns from
        training_targets: A ..... containing the coordinates of the answer
    """

    periods = 10
    steps_per_period = steps/periods

    # Create DNNRegressor object

    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
   # my_optimizer = tf.train.AdagradOptimizer()..... # USE THIS MAYBE?
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
    dnn_regressor = tf.estimator.DNNRegressor(
        feature_columns=construct_feature_column(training_examples),
        hidden_units=hidden_units,
        optimizer=my_optimizer,
        model_dir=checkpointName
    )

    # Create Input fn
    training_input_fn = lambda: my_input_function(training_examples, training_targets[targetLabel], batch_size=batch_size)

    predict_training_input_fn = lambda:my_input_function(training_examples, training_targets[targetLabel], num_epochs=1, shuffle=False)

    predict_validation_input_fn = lambda:my_input_function(validation_examples, validation_targets[targetLabel], num_epochs=1, shuffle=False)

    # Train the model in a loop so the progress can be assessed

    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())
        print("Training model...")
        print("RMSE (on training data):")
        training_rsme = []
        validation_rsme = []
        for period in range(0, periods):
            # Train model from the previous state
            dnn_regressor.train(input_fn=training_input_fn,steps=steps_per_period)

            # Take a break and compute predictions at this point in the training
            training_predictions = dnn_regressor.predict(input_fn=predict_training_input_fn)
            training_predictions = np.array([item['predictions'][0] for item in training_predictions])

            validation_predicitons = dnn_regressor.predict(input_fn=predict_validation_input_fn)
            validation_predicitons = np.array([item['predictions'][0] for item in validation_predicitons])
            print(validation_predicitons)
            # Compute training and validation loss
            training_root_mean_squared_error = math.sqrt(metrics.mean_squared_error(training_predictions,training_targets))
            validation_root_mean_squared_error = math.sqrt(metrics.mean_squared_error(validation_predicitons, validation_targets))

            # Occasionally print the current loss.
            print("  period %02d : %0.2f" %(period, training_root_mean_squared_error))

            # Add loss metrics to arrays for later analysis
            training_rsme.append(training_root_mean_squared_error)
            validation_rsme.append(validation_root_mean_squared_error)



        print("Model Training Finished. Saving Model")

        # Output a graphs of loss metric over periods.
        plt.ylabel("RMSE")
        plt.xlabel("Periods")
        plt.title("Root Mean Squared Error vs. Periods")
        plt.tight_layout()
        plt.plot(training_rsme, label="training")
        plt.plot(validation_rsme, label="validation")
        plt.legend()
        #plt.show()
        print("Final RMSE (on training data):   %0.2f" % training_root_mean_squared_error)
        print("Final RMSE (on validation data): %0.2f" % validation_root_mean_squared_error)
        file_obj = open("C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\NNLog.txt", "a")
        file_obj.write(checkpointName + "- Validation RMSE:" + str(validation_root_mean_squared_error))
        file_obj.close()
        return dnn_regressor, validation_root_mean_squared_error


def main():

    # Setup the input and train NN for predicting opto X


    error = 10
    learning_rate_optoX = 0.00001
    while (error > 2):
     print("Training optoX")
     print("Retraining Model. Error too large.")
     shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoX2.ckpt', ignore_errors=True,onerror=None)  # Deletes previous version
     training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_X")
     validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_X")
     optoX, error = train_nn_regression_model(learning_rate=learning_rate_optoX, steps=3000, batch_size=50,targetLabel="Opto_X",hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                            training_targets=training_targets, validation_examples=validation_examples,
                                            validation_targets=validation_targets, checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoX2.ckpt')
     learning_rate_optoX = learning_rate_optoX * 10
     print("Learning rate updated to: " + str(learning_rate_optoX))


    # Setup the input and train NN for predicting opto Y

    error = 10
    learning_rate_optoY = 0.001
    while (error > 5):
        print("Training optoY")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoY2.ckpt', ignore_errors=True, onerror=None)  # Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_Y")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_Y")
        optoY, error = train_nn_regression_model(learning_rate=learning_rate_optoY, steps=3000, batch_size=50, targetLabel="Opto_Y",
                                            hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                            training_targets=training_targets, validation_examples=validation_examples,
                                            validation_targets=validation_targets,
                                            checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoY2.ckpt')
        learning_rate_optoY = learning_rate_optoY * 10
        print("Learning rate updated to: " + str(learning_rate_optoY))

    # Setup the input and train NN for predicting opto Z

    error = 10
    learning_rate_optoZ = 0.00001
    while (error > 5):
        print("Training optoZ")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoZ2.ckpt', ignore_errors=True,onerror=None)  # Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_Z")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_Z")
        optoZ, error = train_nn_regression_model(learning_rate=learning_rate_optoZ, steps=3000, batch_size=50, targetLabel="Opto_Z",
                                          hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                          training_targets=training_targets, validation_examples=validation_examples,
                                          validation_targets=validation_targets,
                                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoZ2.ckpt')
        learning_rate_optoZ = learning_rate_optoZ * 10
        print("Learning rate updated to: " + str(learning_rate_optoZ))

    # Setup the input and train NN for predicting opto W

    error = 10
    learning_rate_optoW = 0.00001
    while(error > 0.75):
        print("Training optoW")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\\optoW2.ckpt', ignore_errors=True, onerror=None) #Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_W")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_W")
        optoW, error = train_nn_regression_model(learning_rate=learning_rate_optoW, steps=3000, batch_size=50, targetLabel="Opto_W",
                                          hidden_units=[30, 30, 30, 10], training_examples=training_examples,
                                          training_targets=training_targets, validation_examples=validation_examples,
                                          validation_targets=validation_targets,
                                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoW2.ckpt')
        learning_rate_optoW = learning_rate_optoW*10
        print("Learning rate updated to: " + str(learning_rate_optoW))

    # Setup the input and train NN for predicting opto Qx

    error = 10
    learning_rate_optoQx = 0.00001
    while (error > 0.5):
        print("Training optoQx")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoQx2.ckpt', ignore_errors=True,onerror=None)  # Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_Qx")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_Qx")
        optoQx, error = train_nn_regression_model(learning_rate=learning_rate_optoQx, steps=3000, batch_size=50, targetLabel="Opto_Qx",
                                          hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                          training_targets=training_targets, validation_examples=validation_examples,
                                          validation_targets=validation_targets,
                                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQx2.ckpt')
        learning_rate_optoQx = learning_rate_optoQx * 10
        print("Learning rate updated to: " + str(learning_rate_optoQx))

    # Setup the input and train NN for predicting opto Qy

    error = 10
    learning_rate_optoQy = 0.00001
    while (error > 0.5):
        print("Training optoQy")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoQy2.ckpt', ignore_errors=True,onerror=None)  # Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_Qy")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_Qy")
        optoQy, error = train_nn_regression_model(learning_rate=learning_rate_optoQy, steps=3000, batch_size=50, targetLabel="Opto_Qy",
                                          hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                          training_targets=training_targets, validation_examples=validation_examples,
                                          validation_targets=validation_targets,
                                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQy2.ckpt')
        learning_rate_optoQy = learning_rate_optoQy * 10
        print("Learning rate updated to: " + str(learning_rate_optoQy))

    # Setup the input and train NN for predicting opto Qz

    error = 10
    learning_rate_optoQz = 0.00001
    while (error > 0.5):
        print("Training optoQz")
        print("Retraining Model. Error too large.")
        shutil.rmtree('C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Saved Model\optoQz2.ckpt', ignore_errors=True,onerror=None)  # Deletes previous version
        training_targets = preprocess_targets(nnDataHolder.head(40000), "Opto_Qz")
        validation_targets = preprocess_targets(nnDataHolder.tail(10000), "Opto_Qz")
        optoQz, error = train_nn_regression_model(learning_rate=learning_rate_optoQz, steps=3000, batch_size=50, targetLabel="Opto_Qz",
                                          hidden_units=[10, 10, 10, 10], training_examples=training_examples,
                                          training_targets=training_targets, validation_examples=validation_examples,
                                          validation_targets=validation_targets,
                                          checkpointName='C:/Users/cjl/Desktop/NN Repo/Neural Network/Saved Model/optoQz2.ckpt')
        learning_rate_optoQz = learning_rate_optoQz * 10
        print("Learning rate updated to: " + str(learning_rate_optoQz))

    print("Predict on incoming Data")
    currentX = optoX.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentY = optoY.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentZ = optoZ.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentW = optoW.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentQx = optoQx.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentQy = optoQy.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))
    currentQz = optoQz.predict(input_fn=lambda: my_input_function(prediction_examples, prediction_targets))

    print("Predict on data")

    OptoXpred = np.array(list(islice(currentX, 1)))
    OptoYpred = np.array(list(islice(currentY, 1)))
    OptoZpred = np.array(list(islice(currentZ, 1)))
    OptoWpred = np.array(list(islice(currentW, 1)))
    OptoQxpred = np.array(list(islice(currentQx, 1)))
    OptoQypred = np.array(list(islice(currentQy, 1)))
    OptoQzpred = np.array(list(islice(currentQz, 1)))

    print(OptoXpred)
    print(OptoYpred)
    print(OptoZpred)
    print(OptoWpred)
    print(OptoQxpred)
    print(OptoQypred)
    print(OptoQzpred)

main()