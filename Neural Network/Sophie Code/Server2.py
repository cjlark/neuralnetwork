import socket
import sys
import pandas as pd
pd.__version__
import os
import socket, pickle

def SORT_DATA(data):
    #pickle unpackages the DataFrame so that it can be recieved
    d = pickle.loads(data)
    df_str = str(d)
    data = str(df_str[45:])
    data = data.replace("  ",",")
    data = str(data+":")
    return(data)

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 222
    try:
        s.bind((host, port))
    except socket.error:
        print("Binding Failed")
        sys.exit()
    s.listen(4)
    print("")
    print("Listening for client . . .")
    conn, address = s.accept()
    print("Connected to client at ", address)
    print("")
    while True:
        output = conn.recv(2048)
        if output:
            data = SORT_DATA(output)
            print(data)
        else:
            break

if __name__ == "__main__":
    main()