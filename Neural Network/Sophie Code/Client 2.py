import socket
import sys
import os
import pandas as pd
pd.__version__
import socket, pickle


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 2005
host = "127.0.0.1"
s.connect((host,port))

print("I am connected")
d = "3.4,5.8,4.32,4.6,2.53,25.7,2.8:9.3,3.5,5.2,6.8,4.6,7.8,9.6,5.2,6.8,4.6,9.6,"
d = d.split(":")
d = d[0].split(",")
s1 = pd.Series(d)
df = pd.DataFrame(dict(s1=s1))
df = df.transpose()
print("Data Sent: \n",df)

#pickle packages the DataFrame so that it can be sent through socket
data_string = pickle.dumps(df)
#while(True):
s.send(data_string)