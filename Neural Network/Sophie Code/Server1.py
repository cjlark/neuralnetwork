import socket
import sys
import pandas as pd
pd.__version__
import os

def SORT_DATA(d):
    #data comes in as bytes- converted to string
    d = d.decode()
    d = d.split(":")
    d = d[0].split(",")
    s1 = pd.Series(d)
    df = pd.DataFrame(dict(s1 = s1))
    df = df.transpose()
    return (df)

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 222
    try:
        s.bind((host, port))
    except socket.error:
        print("Binding Failed")
        sys.exit()
    s.listen(4)
    print("")
    print("Listening for client . . .")
    conn, address = s.accept()
    print("Connected to client at ", address)
    print("")
    while True:
        output = conn.recv(2048)
        if output:
            sorted = SORT_DATA(output)
            print(sorted)
        else:
            break

if __name__ == "__main__":
    main()