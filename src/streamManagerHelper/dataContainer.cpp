#include "dataContainer.h"


void NeuralDataContainer::setOptoTrans(double x, double y, double z) {
	optoTrans[0] = x;
	optoTrans[1] = y;
	optoTrans[2] = z;
}

void NeuralDataContainer::setKukaTrans(double x, double y, double z) {
	kukaTrans[0] = x;
	kukaTrans[1] = y;
	kukaTrans[2] = z;
}

void NeuralDataContainer::setNdiTrans(double x, double y, double z) {
	ndiTrans[0] = x;
	ndiTrans[1] = y;
	ndiTrans[2] = z;
}

void NeuralDataContainer::setOptoRot(double rot[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			optoRot[i][ii] = rot[i][ii];
		}
	}
}

void NeuralDataContainer::setKukaRot(double rot[3]) {
	kukaRot[0] = rot[0];
	kukaRot[1] = rot[1];
	kukaRot[2] = rot[2];
}

void NeuralDataContainer::setNDIRot(double rot[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			ndiRot[i][ii] = rot[i][ii];
		}
	}
}

void NeuralDataContainer::setNDIDif(double origTrans[3], double origRot[3][3]) {
	for (int i = 0; i < 3; i++) {
		NDItransDif[i] = ndiTrans[i] - origTrans[i];
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			NDIrotDif[i][j] = ndiRot[i][j] - origRot[i][j];
		}
	}
}

void NeuralDataContainer::setOptoDif(double origTrans[3], double origRot[3][3]) {
	for (int i = 0; i < 3; i++) {
		OptotransDif[i] = optoTrans[i] - origTrans[i];
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			OptorotDif[i][j] = optoRot[i][j] - origRot[i][j];
		}
	}
}

void NeuralDataContainer::setKukaTs(long long timestamp) { //this is the system time when the data came in
	kukaTs = timestamp;
}

void NeuralDataContainer::setNdiTs(long long timestamp) {
	ndiTs = timestamp;
}

void NeuralDataContainer::copyKukaData(NeuralDataContainer dataToCopy, long long timestamp) {
	dataToCopy.getKukaRot(this->kukaRot);
	dataToCopy.getKukaTrans(this->kukaTrans);
	this->timeSinceLastKuka = timestamp - dataToCopy.kukaTs;
}

void NeuralDataContainer::copyNdiData(NeuralDataContainer dataToCopy, long long timestamp) {
	dataToCopy.getNDIRot(this->ndiRot);
	dataToCopy.getNDITrans(this->ndiTrans);
	this->ndiQuality = dataToCopy.ndiQuality;
	this->timeSinceLastNdi = timestamp - dataToCopy.ndiTs;
}


void NeuralDataContainer::getOptoTrans(double transToFill[3]) {
	transToFill[0] = optoTrans[0];
	transToFill[1] = optoTrans[1];
	transToFill[2] = optoTrans[2];
}

void NeuralDataContainer::getKukaTrans(double transToFill[3]) {
	transToFill[0] = kukaTrans[0];
	transToFill[1] = kukaTrans[1];
	transToFill[2] = kukaTrans[2];
}

void NeuralDataContainer::getNDITrans(double transToFill[3]) {
	transToFill[0] = ndiTrans[0];
	transToFill[1] = ndiTrans[1];
	transToFill[2] = ndiTrans[2];
}

void NeuralDataContainer::getOptoRot(double rotToFill[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			rotToFill[i][ii] = optoRot[i][ii];
		}
	}
}

void NeuralDataContainer::getKukaRot(double rotToFill[3]) {
	rotToFill[0] = kukaRot[0];
	rotToFill[1] = kukaRot[1];
	rotToFill[2] = kukaRot[2];
}

void NeuralDataContainer::getNDIRot(double rotToFill[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			rotToFill[i][ii] = ndiRot[i][ii];
		}
	}
}

/*
Returns a comma separated string of all the kuka data
*/
std::string NeuralDataContainer::getAllKukaData() {
	std::string kukaString = "";

	kukaString += std::to_string(kukaTrans[0]);
	kukaString += ",";
	kukaString += std::to_string(kukaTrans[1]);
	kukaString += ",";
	kukaString += std::to_string(kukaTrans[2]);
	kukaString += ",";

	kukaString += std::to_string(kukaRot[0]);
	kukaString += ",";
	kukaString += std::to_string(kukaRot[1]);
	kukaString += ",";
	kukaString += std::to_string(kukaRot[2]);
	kukaString += ",";
	kukaString += std::to_string(timeSinceLastKuka);

	return kukaString;
}

/*
Returns a comma separated string of all the opto data
*/
std::string NeuralDataContainer::getAllOptoData() {
	std::string optoString = "";

	optoString += std::to_string(optoTrans[0]);
	optoString += ",";
	optoString += std::to_string(optoTrans[1]);
	optoString += ",";
	optoString += std::to_string(optoTrans[2]);
	optoString += ",";

	/*optoString += std::to_string(optoQuat[0]);
	optoString += ",";
	optoString += std::to_string(optoQuat[1]);
	optoString += ",";
	optoString += std::to_string(optoQuat[2]);
	optoString += ",";
	optoString += std::to_string(optoQuat[3]);*/

	return optoString;
}

/*
Returns a comma separated string of all the ndi data
*/
std::string NeuralDataContainer::getAllNDIData() {
	std::string NDIString = "";

	NDIString += std::to_string(ndiTrans[0]);
	NDIString += ",";
	NDIString += std::to_string(ndiTrans[1]);
	NDIString += ",";
	NDIString += std::to_string(ndiTrans[2]);
	NDIString += ",";

	NDIString += std::to_string(ndiRot[0][0]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[0][1]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[0][2]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[1][0]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[1][1]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[1][2]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[2][0]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[2][1]);
	NDIString += ",";
	NDIString += std::to_string(ndiRot[2][2]);
	NDIString += ",";
	NDIString += std::to_string(ndiQuality);
	//NDIString += ",";
	//NDIString += std::to_string(timeSinceLastNdi);

	return NDIString;
}

//std::string NeuralDataContainer::getAllNDIDif() {
//	std::string difString = "";
//	difString += std::to_string(NDItransDif[0]);
//	difString += ",";
//	difString += std::to_string(NDItransDif[1]);
//	difString += ",";
//	difString += std::to_string(NDItransDif[2]);
//	difString += ",";
//
//	difString += std::to_string(NDIrotDif[0][0]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[0][1]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[0][2]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[1][0]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[1][1]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[1][2]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[2][0]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[2][1]);
//	difString += ",";
//	difString += std::to_string(NDIrotDif[2][2]);
//	
//
//	return difString;
//}
//
//std::string NeuralDataContainer::getAllOptoDif() {
//	std::string difString = "";
//	difString += std::to_string(OptotransDif[0]);
//	difString += ",";
//	difString += std::to_string(OptotransDif[1]);
//	difString += ",";
//	difString += std::to_string(OptotransDif[2]);
//	difString += ",";
//
//	difString += std::to_string(OptorotDif[0][0]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[0][1]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[0][2]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[1][0]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[1][1]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[1][2]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[2][0]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[2][1]);
//	difString += ",";
//	difString += std::to_string(OptorotDif[2][2]);
//
//	return difString;
//}

Eigen::Matrix4f NeuralDataContainer::getNDITransMat() {
	Eigen::Matrix4f transMat = Eigen::Matrix4f::Identity();

	transMat(0, 0) = ndiRot[0][0];
	transMat(0, 1) = ndiRot[0][1];
	transMat(0, 2) = ndiRot[0][2];
	transMat(1, 0) = ndiRot[1][0];
	transMat(1, 1) = ndiRot[1][1];
	transMat(1, 2) = ndiRot[1][2];
	transMat(2, 0) = ndiRot[2][0];
	transMat(2, 1) = ndiRot[2][1];
	transMat(2, 2) = ndiRot[2][2];

	transMat(0, 3) = ndiTrans[0];
	transMat(1, 3) = ndiTrans[1];
	transMat(2, 3) = ndiTrans[2];
	return transMat;
}

void NeuralDataContainer::getNDIDif(double Trans[3], double Rot[3][3]) {
	for (int i = 0; i < 3; i++) {
		Trans[i] = NDItransDif[i];
	}

	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			Rot[i][ii] = NDIrotDif[i][ii];
		}
	}

}

void NeuralDataContainer::getOptoDif(double Trans[3], double Rot[3][3]) {
	for (int i = 0; i < 3; i++) {
		Trans[i] = OptotransDif[i];
	}

	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			Rot[i][ii] = OptorotDif[i][ii];
		}
	}

}

long long NeuralDataContainer::getKukaTSL() {
	return timeSinceLastKuka;
}

