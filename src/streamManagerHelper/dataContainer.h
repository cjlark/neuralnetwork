#pragma once
#include <string>
#include <vector>
#include <array>
#include <pcl/common/transforms.h>
class NeuralDataContainer {

public:

	double ndiQuality;
	double optoTrans[3];
	double kukaTrans[3];
	double ndiTrans[3];
	double optoRot[3][3];
	double kukaRot[3];
	double ndiRot[3][3];
	double NDItransDif[3];
	double NDIrotDif[3][3];
	double OptotransDif[3];
	double OptorotDif[3][3];

	long long kukaTs;
	long long ndiTs;

	NeuralDataContainer() {
		timeSinceLastKuka = 0;
		timeSinceLastNdi = 0;
		ndiQuality = 0;
	}

	void setOptoTrans(double x, double y, double z);
	void setKukaTrans(double x, double y, double z);
	void setNdiTrans(double x, double y, double z);
	void setOptoRot(double rot[3][3]);
	void setKukaRot(double rot[3]);
	void setNDIRot(double rot[3][3]);
	//trans and rot are the original sensor position and orientation
	void setNDIDif(double origTrans[3], double origRot[3][3]);
	void setOptoDif(double origTrans[3], double origRot[3][3]);

	void setKukaTs(long long timestamp); //this is the system time when the data came in
	void setNdiTs(long long timestamp);

	void copyKukaData(NeuralDataContainer dataToCopy, long long timestamp);
	void copyNdiData(NeuralDataContainer dataToCopy, long long timestamp);

	void getOptoTrans(double transToFill[3]);
	void getKukaTrans(double transToFill[3]);
	void getNDITrans(double transToFill[3]);
	void getOptoRot(double rotToFill[3][3]);
	void getKukaRot(double rotToFill[3]);
	void getNDIRot(double rotToFill[3][3]);
	void getNDIDif(double Trans[3], double Rot[3][3]);
	void getOptoDif(double Trans[3], double Rot[3][3]);

	std::string getAllKukaData();
	std::string getAllOptoData();
	std::string getAllNDIData();
	long long getKukaTSL();
	Eigen::Matrix4f getNDITransMat();

private:
	long long timeSinceLastKuka;
	long long timeSinceLastNdi;
};