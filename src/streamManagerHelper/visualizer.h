#pragma once
#include <iostream>
#include "BoostSoc.h"
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>

class Visualizer
{
public:
	//Viewer that takes in a socket and existing viewer and 
	boost::shared_ptr<pcl::visualization::PCLVisualizer> createViewer(std::string viewerName, double coordinateScale);
	void neuralNetworkVisualizer(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, Eigen::Matrix4f transMat, std::string frameName);
	void drawFrame(Eigen::Matrix4f &frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer);
private:

	
	void rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle);
	void translate(Eigen::Matrix4f &inFrame, Eigen::RowVector3f vec);
};