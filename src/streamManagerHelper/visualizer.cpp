#include "visualizer.h"


boost::shared_ptr<pcl::visualization::PCLVisualizer> Visualizer::createViewer(std::string viewerName, double coordinateScale) {
	boost::shared_ptr<pcl::visualization::PCLVisualizer> tempViewer(new pcl::visualization::PCLVisualizer(viewerName));
	tempViewer->setBackgroundColor(0, 0, 0);
	tempViewer->addCoordinateSystem(coordinateScale);
	tempViewer->initCameraParameters();
	tempViewer->spinOnce();
	return tempViewer;
}

void Visualizer::neuralNetworkVisualizer(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, Eigen::Matrix4f transMat, std::string frameName) {
	drawFrame(transMat, frameName, viewer);
	viewer->spinOnce(5);
}

void Visualizer::drawFrame(Eigen::Matrix4f &frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer)
{
	//this draws a cone to visualize our point in space. ndi software can give us values for point locations and normals. once we have values for normals, I can make the cone tip point in the normal direction 

	pcl::ModelCoefficients xAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients yAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();

	//x,y,z,dx,dy,dz,angle
	xAxis.values = std::vector<float>{ 0, 0, 0, 10, 0, 0, 10 };
	yAxis.values = std::vector<float>{ 0, 0, 0, 0, 10, 0, 10 };
	zAxis.values = std::vector<float>{ 0, 0, 0, 0, 0, 10, 10 };

	std::string xName = name + "_x";
	if (!visualizer->contains(xName))
	{
		visualizer->addCone(xAxis, xName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, xName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, xName);
	}
	visualizer->updateShapePose(xName, Eigen::Affine3f(frame));

	std::string yName = name + "_y";
	if (!visualizer->contains(yName))
	{
		visualizer->addCone(yAxis, yName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0.5, 0, yName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, yName);
	}
	visualizer->updateShapePose(yName, Eigen::Affine3f(frame));

	std::string zName = name + "_z";
	if (!visualizer->contains(zName))
	{
		visualizer->addCone(zAxis, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	visualizer->updateShapePose(zName, Eigen::Affine3f(frame));

}

void Visualizer::rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle)
{
	//https://sites.google.com/site/glennmurray/Home/rotation-matrices-and-formulas/rotation-about-an-arbitrary-axis-in-3-dimensions
	Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();

	dir.normalize();

	//pt: a,b,c
	//dir: u,v,w
	float a = pt(0);
	float b = pt(1);
	float c = pt(2);

	float u = dir(0);
	float v = dir(1);
	float w = dir(2);

	transformation(0, 0) = u * u + (v * v + w * w) * cos(angle);
	transformation(0, 1) = u * v * (1.0 - cos(angle)) - w * sin(angle);
	transformation(0, 2) = u * w * (1.0 - cos(angle)) + v * sin(angle);
	transformation(0, 3) = (a * (v * v + w * w) - u * (b * v + c * w)) * (1.0 - cos(angle)) + (b * w - c * v) * sin(angle);

	transformation(1, 0) = u * v * (1.0 - cos(angle)) + w * sin(angle);
	transformation(1, 1) = v * v + (u * u + w * w) * cos(angle);
	transformation(1, 2) = v * w * (1.0 - cos(angle)) - u * sin(angle);
	transformation(1, 3) = (b * (u * u + w * w) - v * (a * u + c * w)) * (1.0 - cos(angle)) + (c * u - a * w) * sin(angle);

	transformation(2, 0) = u * w * (1.0 - cos(angle)) - v * sin(angle);
	transformation(2, 1) = v * w * (1.0 - cos(angle)) + u * sin(angle);
	transformation(2, 2) = w * w + (u * u + v * v) * cos(angle);
	transformation(2, 3) = (c * (u * u + v * v) - w * (a * u + b * v)) * (1.0 - cos(angle)) + (a * v - b * u) * sin(angle);

	transformation(3, 0) = 0;
	transformation(3, 1) = 0;
	transformation(3, 2) = 0;
	transformation(3, 3) = 1.0;

	inFrame = transformation * inFrame;
}

void Visualizer::translate(Eigen::Matrix4f &inFrame, Eigen::RowVector3f vec)
{
	inFrame(0, 3) += vec(0);
	inFrame(1, 3) += vec(1);
	inFrame(2, 3) += vec(2);
}
