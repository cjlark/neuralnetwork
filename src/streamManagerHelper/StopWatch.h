#pragma once

#include <chrono>
#include <string>
#include <iostream>

typedef std::chrono::steady_clock Clock;
using namespace std;
using namespace std::chrono;

class StopWatch
{
public:
	StopWatch() {
		mainClock = std::chrono::steady_clock::now();
	}

	StopWatch(long long _maxStamps) {
		mainClock = std::chrono::steady_clock::now();
		max_stamps = _maxStamps;
		stamps = (time_point<steady_clock> *)malloc(_maxStamps * sizeof(time_point<steady_clock>));
	}

	void reset() {
		free(stamps);
		mainClock = std::chrono::steady_clock::now();
		lapDuration = std::chrono::duration<double>::zero();
		totalDuration = std::chrono::duration<double>::zero();
		shortLap = std::chrono::duration<double>::max();
		longLap = std::chrono::duration<double>::min();
		lapCount = 0;
	}

	double checkDuration() {
		std::chrono::duration<double> duration = (std::chrono::steady_clock::now() - mainClock);
		return duration.count();
	}

	void lap() {
		tallyLap();
	}

	void stampedLap(string stamp) {
		tallyLap();
		std::cout << stamp << " : " << durationCast(lapDuration).count() << " uS\n";
	}

	void stampedStop() {
		tallyLap();
		std::cout << "---Full Stamp---\n";
		std::cout << "Total Duration: " << durationCast(totalDuration).count() << " uS\n";
		std::cout << "Average Lap: " << durationCast(totalDuration).count()/(double)lapCount << " uS\n";
		std::cout << "High Lap: " << durationCast(longLap).count() << " uS\n";
		std::cout << "Low Lap: " << durationCast(shortLap).count() << " uS\n";
	}

private:
	long long max_stamps = 0;
	long long currentStampIndex = 0;
	std::chrono::time_point<std::chrono::steady_clock> mainClock;
	std::chrono::duration<double> lapDuration = std::chrono::duration<double>::zero();
	std::chrono::duration<double> totalDuration = std::chrono::duration<double>::zero();
	std::chrono::duration<double> shortLap = std::chrono::duration<double>::max();
	std::chrono::duration<double> longLap = std::chrono::duration<double>::min(); //worst case scenario
	int lapCount = 0;
	time_point<steady_clock> *stamps;
	
	std::chrono::microseconds durationCast(std::chrono::duration<double> dur) {
		std::chrono::microseconds returnDur = std::chrono::duration_cast<std::chrono::microseconds>(dur);
		return returnDur;
	}

	void tallyLap() {
		lapDuration = std::chrono::steady_clock::now() - mainClock;
		mainClock = std::chrono::steady_clock::now();
		totalDuration += lapDuration;
		shortLap = lapDuration < shortLap ? lapDuration : shortLap;
		longLap = lapDuration > longLap ? lapDuration : longLap;
		lapCount++;
	}

	void fastTally() {
		if (currentStampIndex >= max_stamps) {}
		stamps[currentStampIndex] = steady_clock::now();
		currentStampIndex++;
	}
};