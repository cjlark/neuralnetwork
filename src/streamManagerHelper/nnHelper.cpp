#include "nnHelper.h"

void connectSocket(BoostSoc& server, int port, std::string name, int& status) {
	std::cout << name << " not connected!\nAttempting...\n";
	server.serverConnect(port, true);
	while (!server.getConnected()) { // While the client is not connected, print out a message letting the user know.

		std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // Sleep for 1 second between message prints
	}
	status = 1;
}

void parseUIMessage(std::string fullString, std::string& fileName, double& numSamples, double& sampleRate, std::string& collectionType) {

	int count = 0;
	char c;

	std::string tempString = "";
	for (int i = 0; i < fullString.length(); i++) {
		if (fullString[i] != ',' && fullString[i] != ';') {
			tempString += fullString[i];
		}
		else {

			if (count == 0) {
				fileName = tempString;
			}
			else if (count == 1) { //its output should be a number
				numSamples = std::stoi(tempString);
			}
			else if (count == 2) {
				sampleRate = std::stoi(tempString);
			}
			else {
				collectionType = tempString;
			}
			tempString = "";
			count++;
		}
	}
}

std::vector<double> parseString(std::string incomingData) {
	vector<string> holder;
	vector<double> data;
	boost::split(holder, incomingData, boost::is_any_of(","), boost::token_compress_on);
	for (int i = 0; i < holder.size(); i++) {
		data.push_back(stod(holder.at(i)));
	}
	return data;
}

/*
Receives Quaternion + translation and converts into a transformation matrix;
*/
Eigen::Matrix4f quatToMatEigen(std::vector<double> quaternion) {
	// Conversion math taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm

	Eigen::Quaterniond quat;
	quat.w() = quaternion.at(3);
	quat.x() = quaternion.at(4);
	quat.y() = quaternion.at(5);
	quat.z() = quaternion.at(6);

	Eigen::Matrix3d rotMat = quat.normalized().toRotationMatrix();

	Eigen::Matrix4f transMat = Eigen::Matrix4f::Identity();
	transMat(0, 3) = quaternion.at(0);
	transMat(1, 3) = quaternion.at(1);
	transMat(2, 3) = quaternion.at(2);

	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			transMat(i, ii) = rotMat(i, ii);
		}
	}

	return transMat;
}


Eigen::Matrix4f stringToTransMat(std::string matrixString) {
	std::vector<double> holder = parseString(matrixString);
	Eigen::Matrix4f transMat;

	transMat(0, 3) = holder.at(0);
	transMat(1, 3) = holder.at(1);
	transMat(2, 3) = holder.at(2);

	transMat(0, 0) = holder.at(3);
	transMat(0, 1) = holder.at(4);
	transMat(0, 2) = holder.at(5);
	transMat(1, 0) = holder.at(6);
	transMat(1, 1) = holder.at(7);
	transMat(1, 2) = holder.at(8);
	transMat(2, 0) = holder.at(9);
	transMat(2, 1) = holder.at(10);
	transMat(2, 2) = holder.at(11);

	return transMat;
}

void monitorInput(bool& flag, bool& calc, std::string desiredInput) {
	string input = "";
	while (true) {
		cin >> input;
		if (input == desiredInput) {
			flag = !flag;
		}
		else if (input == "c") {
			calc = true;
		}

	}
}

Eigen::Matrix4f getMatDif(Eigen::Matrix4f& originalMat, Eigen::Matrix4f& difMat) {

	for (int i = 0; i < 3; i++) {
		originalMat(i, 3) = originalMat(i, 3) - difMat(i, 3);
	}

	for (int i = 0; i < 3; i++) {
		for (int ii = 0; ii < 3; ii++) {
			originalMat(i, ii) = originalMat(i, ii) - difMat(i, ii);
		}
	}
	return originalMat;
}

std::string dataToSend(double ndiTransDif[3], double ndiRotDif[3][3], NeuralDataContainer& current) {
	string currentData = "";
	currentData += to_string(ndiTransDif[0]);
	currentData += ",";
	currentData += to_string(ndiTransDif[1]);
	currentData += ",";
	currentData += to_string(ndiTransDif[2]);
	currentData += ",";
	currentData += to_string(ndiRotDif[0][0]);
	currentData += ",";
	currentData += to_string(ndiRotDif[0][1]);
	currentData += ",";
	currentData += to_string(ndiRotDif[0][2]);
	currentData += ",";
	currentData += to_string(ndiRotDif[1][0]);
	currentData += ",";
	currentData += to_string(ndiRotDif[1][1]);
	currentData += ",";
	currentData += to_string(ndiRotDif[1][2]);
	currentData += ",";
	currentData += to_string(ndiRotDif[2][0]);
	currentData += ",";
	currentData += to_string(ndiRotDif[2][1]);
	currentData += ",";
	currentData += to_string(ndiRotDif[2][2]);
	currentData += ",";
	currentData += current.getAllNDIData();
	currentData += ",";
	currentData += current.getAllKukaData();

	return currentData;
}

std::string combineString(std::vector<std::string> holder) {
	string completeString = "";
	for (int i = 0; i < holder.size() - 1; i++) {
		completeString += holder.at(i);
		completeString += ",";
	}
	//To make sure a ',' was not added to the end of the string.
	completeString += holder.at(holder.size() - 1);

	return completeString;
}