#pragma once
#include "BoostSoc.h"
#include "StopWatch.h"
#pragma once
#include <thread>
#include <fstream>
#include <ctime>
#include <mutex>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_lib_io.h> 
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/Geometry>
#include <deque>
#include "dataContainer.h"


// Function to connect the client end for a given stream
void connectSocket(BoostSoc& server, int port, std::string name, int& status);
// Parses through the incoming UI message and assigns the values
void parseUIMessage(std::string fullString, std::string& fileName, double& numSamples, double& sampleRate, std::string& collectionType);
// Takes a vector of quaternion values and converts to a transformation matrix
std::vector<double> parseString(std::string incomingData);
Eigen::Matrix4f quatToMatEigen(std::vector<double> quaternion);
Eigen::Matrix4f stringToTransMat(std::string matrixString);
void monitorInput(bool& flag, bool& calc, std::string desiredInput);
//calculates the difference between originalMat and difMat in place
Eigen::Matrix4f getMatDif(Eigen::Matrix4f& originalMat, Eigen::Matrix4f& difMat);
std::string dataToSend(double ndiTransDif[3], double ndiRotDif[3][3], NeuralDataContainer& current);
std::string combineString(std::vector<std::string> holder);