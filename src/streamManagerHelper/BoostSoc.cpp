#include "BoostSoc.h"

void BoostSoc::serverConnect(int port, bool blocking)
{
	if (listening || connected) { return; }

	if (!blocking)
	{
		boost::thread backCon(boost::bind(&BoostSoc::serverConnectDoWork, this, port));
	}
	else
	{
		this->serverConnectDoWork(port);
	}
}

void BoostSoc::clientConnect(int port, std::string ip, bool blocking, int connectionAttempts)
{
	if (listening || connected) { return; }

	using boost::asio::ip::tcp;
	tcp::resolver resolver(io_service);
	tcp::resolver::query query(tcp::v4(), ip, std::to_string(port));
	tcp::resolver::iterator iterator = resolver.resolve(query);

	if (!blocking)
	{
		boost::thread backCon(boost::bind(&BoostSoc::clientConnectDoWork, this, iterator,connectionAttempts));
	}
	else
	{
		this->clientConnectDoWork(iterator,connectionAttempts);
	}
}

std::string BoostSoc::getMessage()
{
	if (!connected) { return ""; }

	try
	{
		std::size_t count = connectedSocket.available();
		static const int size = 50000;
		if (count > 0)
		{
			char inMessage[size];
			boost::asio::read(connectedSocket, boost::asio::buffer(inMessage, count));
			if (count > max_Size) { count = max_Size; }
			std::string trimmedMessage = std::string(inMessage, count);
			return trimmedMessage;
		}
		else
		{
			return "";
		}
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
		return "";
	}
}

void BoostSoc::sendMessage(std::string message)
{
	if (!connected) { return; }

	try
	{
		boost::asio::write(connectedSocket, boost::asio::buffer(message.c_str(), message.length()));
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
	}
}

void BoostSoc::serverConnectDoWork(int port)
{
	listening = true;
	try
	{
		using boost::asio::ip::tcp;
		tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
		a.accept(connectedSocket);
		connected = true;
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
		connected = false;
	}
	listening = false;
}

void BoostSoc::clientConnectDoWork(boost::asio::ip::tcp::resolver::iterator iterator, int attempts)
{
	listening = true;
	while (!connected && (attempts == -1 || attempts > 0))
	{
		try
		{
			boost::asio::connect(connectedSocket, iterator);
			connected = true;
		}
		catch (std::exception& e)
		{
			if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
			connected = false;
		}
		if (attempts != -1) { attempts -= 1; }
	}
	listening = false;
}