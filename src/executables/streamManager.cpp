#include "nnHelper.h"
#include "dataContainer.h"
#include <string>
#include <boost/algorithm/string.hpp>
#include "visualizer.h"

BoostSoc kukaClient, optoClient, ndiClient, UIserver, optoServ, ndiServ, kukaServ;;
mutex mtx;
vector<NeuralDataContainer> nnData;
void sendStatus(int optoStatus, int ndiStatus, int kukaStatus);
void monitorStatus(int optoStatus, int ndiStatus, int kukaStatus);
void collectTestingData(double numSamples, std::string ndiString, double ndiTrans[3], double ndiRot[3][3]);
void collectLiveData(int& ndiStatus, int& optoStatus, int& kukaStatus);
void readOpto(NeuralDataContainer& currentData, std::string optoMessage);
void readKuka(NeuralDataContainer& currentData, std::string kukaMessage, long long kukaTs);
void readNdi(NeuralDataContainer& currentData, std::string ndiMessage);
void writeToFile(std::string fileName, double originalTrans[3], double originalRot[3][3]);
void sendOptoMessage(std::string message);
void compOptoNDI(int numSamples);
void writeDifToFile(std::string fileName);

int main()
{
	//kukaServ.clientConnect(2000,"127.0.0.1", false, -1);
	//optoServ.clientConnect(2003, "127.0.0.1", false, -1);
	//ndiServ.clientConnect(2004, "127.0.0.1", false, -1);

	// Connect to the UI
	UIserver.serverConnect(2001, false);
	while (!UIserver.getConnected()) {
		std::cout << "Not Connected to UI!\n";
		std::cout << "Attempting...\n";
		Sleep(1000);
	}

	std::string UIMessage = UIserver.getMessage();
	std::string fileName, collectionType;
	double numSamples, sampleRate;

	while (UIMessage == "") { // Wait until message from the UI
		UIMessage = UIserver.getMessage();
	}

	parseUIMessage(UIMessage, fileName, numSamples, sampleRate, collectionType);
	std::cout << "File name specified as: " << fileName << std::endl;
	int optoStatus = 0, ndiStatus = 0, kukaStatus = 0;
	sendStatus(optoStatus, ndiStatus, kukaStatus); //send intialization message

												   // Determine if doing Testing or Live Capture
	if (collectionType == "test") {
		// Attempt connection to the three streaming devices
		//connectSocket(optoClient, 2003, "optoClient", optoStatus);
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		connectSocket(ndiClient, 2004, "ndiClient", ndiStatus);
		ndiClient.sendMessage("1");
		std::string ndiString = ndiClient.getMessage();
		while (ndiString == "") {
			ndiString = ndiClient.getMessage();
		}
		ndiClient.sendMessage("2");
		std::cout << "Gathered initial NDI Point. Press any key to continue.\n";
		string x;
		cin >> x;
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		connectSocket(kukaClient, 2000, "Kuka", kukaStatus);
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		boost::thread sendStatus(monitorStatus, optoStatus, ndiStatus, kukaStatus);
		Sleep(100);

		//boost::thread optoThread(sendOptoMessage, message);
		//std::cout << "Sending Opto Messages\n";
		// Begin collecting Data and pushing to custom Data Class
		std::cout << "Collecting Data...\n";
		double origNDITrans[3], origNDIRot[3][3];
		collectTestingData(numSamples, ndiString, origNDITrans, origNDIRot);

		std::cout << numSamples << " samples collected!\n";
		std::cout << "Publishing Results...\n";
		writeToFile(fileName, origNDITrans, origNDIRot);
		std::cout << "Data cleaned!\n";

	}
	else if (collectionType == "vs") {
		connectSocket(optoClient, 2003, "optoClient", optoStatus);
		connectSocket(ndiClient, 2004, "ndiClient", ndiStatus);
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		std::cout << "Optotrak and NDI connected. Enter any key to begin testing.\n";
		std::string input;
		std::cin >> input;
		compOptoNDI(numSamples);
		writeDifToFile(fileName);

	}
	else { // This is for a real time feed to the NN
		collectLiveData(ndiStatus, optoStatus, kukaStatus);
	}

	system("pause");
	return 0;
}

/*
Sends the connection status of each stream to the UI
*/
void sendStatus(int optoStatus, int ndiStatus, int kukaStatus) {
	UIserver.sendMessage(std::to_string(optoClient.getConnected()) + "," + std::to_string(ndiClient.getConnected()) + "," + std::to_string(kukaClient.getConnected()));
}

/*
Periodically checks the status of the connections and sends info to UI
*/
void monitorStatus(int optoStatus, int ndiStatus, int kukaStatus) {
	while (true) {
		optoStatus = optoClient.getConnected() ? 1 : 0;
		ndiStatus = ndiClient.getConnected() ? 1 : 0;
		kukaStatus = kukaClient.getConnected() ? 1 : 0;
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

/*
Manages all three data collection systems and pushes to a vector which contains the recorded data
*/
void collectTestingData(double numSamples, std::string ndiString, double ndiTrans[3], double ndiRot[3][3]) {
	int count = 0;
	std::vector<std::string> holder;
	boost::split(holder, ndiString, boost::is_any_of(","), boost::token_compress_on);
	ndiTrans[0] = stod(holder.at(0));
	ndiTrans[1] = stod(holder.at(1));
	ndiTrans[2] = stod(holder.at(2));

	ndiRot[0][0] = stod(holder.at(3));
	ndiRot[0][1] = stod(holder.at(4));
	ndiRot[0][2] = stod(holder.at(5));
	ndiRot[1][0] = stod(holder.at(6));
	ndiRot[1][1] = stod(holder.at(7));
	ndiRot[1][2] = stod(holder.at(8));
	ndiRot[2][0] = stod(holder.at(9));
	ndiRot[2][1] = stod(holder.at(10));
	ndiRot[2][2] = stod(holder.at(11));
	long long kukaTs; 
	std::string ndiMessage = "";
	std::string kukaMessage = "";
	
	ndiClient.sendMessage("1");
	kukaClient.sendMessage("start");

	while (count < numSamples) {
		NeuralDataContainer dataInstance;
		ndiMessage = ndiClient.getMessage();
		
		if (ndiMessage != "") { // If new NDI data is available
			kukaMessage = kukaClient.getMessage();
			kukaTs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
			cout << "Kuka: " << kukaMessage << endl;
			readNdi(dataInstance, ndiMessage);
			readKuka(dataInstance, kukaMessage, kukaTs);
			nnData.push_back(dataInstance);
			count++;
			ndiClient.sendMessage("1");
		}
		//std::this_thread::sleep_for(std::chrono::milliseconds(5));
	}
}

/*
Collects NDI and Kuka data live and feeds it to neural network.
*/
void collectLiveData(int& ndiStatus, int& optoStatus, int& kukaStatus) {
	BoostSoc pythonSoc;
	pythonSoc.clientConnect(2005, "127.0.0.1", false, -1);
	while (!pythonSoc.getConnected()) {
		std::cout << "Attempting to connect to python program...\n";
	}


	bool nnData = false; // This flag determines what type of data should be displayed
	bool calcDif = false;
	bool end = false;
	std::string inputKey = "t";
	boost:thread readInput(monitorInput, boost::ref(nnData), boost::ref(calcDif), inputKey);

	Visualizer vis;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = vis.createViewer("LiveView", 20.0);
	pcl::PolygonMesh table, twoXfour;
	pcl::io::loadPolygonFileSTL("C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Models\\WoodTable.stl", table);
	pcl::io::loadPolygonFileSTL("C:\\Users\\cjl\\Desktop\\NN Repo\\Neural Network\\Models\\2X4.stl", twoXfour);
	viewer->addPolygonMesh(table, "table");
	viewer->addPolygonMesh(twoXfour, "wood");
	//initialize all streams
	std::string sendMessage = "";
	std::string ndiData = "";
	std::string kukaData = "";
	std::string frameString = "";
	Eigen::Matrix4f nnFrame = Eigen::Matrix4f::Identity();
	Eigen::Matrix4f rawFrame = Eigen::Matrix4f::Identity();

	connectSocket(ndiClient, 2004, "ndiClient", ndiStatus);
	sendStatus(optoStatus, ndiStatus, kukaStatus);

	connectSocket(kukaClient, 2000, "Kuka", kukaStatus);
	sendStatus(optoStatus, ndiStatus, kukaStatus);
	ndiClient.sendMessage("1");
	kukaClient.sendMessage("start");
	std::this_thread::sleep_for(std::chrono::milliseconds(2));
	long long kukaTs;
	while (true) {
		NeuralDataContainer dataInstance;
		ndiData = ndiClient.getMessage();
		if (ndiData != "") {
			kukaData = kukaClient.getMessage();
			kukaTs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
			readNdi(dataInstance, ndiData);
			readKuka(dataInstance, kukaData, kukaTs);


			sendMessage += dataInstance.getAllNDIData();
			sendMessage += ",";
			sendMessage += dataInstance.getAllKukaData();

			pythonSoc.sendMessage(sendMessage);
			sendMessage = "";
			frameString = pythonSoc.getMessage();
			while (frameString == "") {
				frameString = pythonSoc.getMessage();
			}
			if (!nnData) {
				nnFrame = getMatDif(dataInstance.getNDITransMat(), stringToTransMat(frameString));
				vis.neuralNetworkVisualizer(viewer, nnFrame, "nnFrame");
				if (calcDif) {
					cout << "X Dif: " << nnFrame(0, 3) - rawFrame(0, 3) << std::endl;
					cout << "Y Dif: " << nnFrame(1, 3) - rawFrame(1, 3) << std::endl;
					cout << "Z Dif: " << nnFrame(2, 3) - rawFrame(2, 3) << std::endl;
					calcDif = false;
					Eigen::Vector3f nnX, nnY, X, Y;
					nnX(0) = nnFrame(0, 0);
					nnX(1) = nnFrame(0, 1);
					nnX(2) = nnFrame(0, 2);

					nnY(0) = nnFrame(1, 0);
					nnY(1) = nnFrame(1, 1);
					nnY(2) = nnFrame(1, 2);

					X(0) = rawFrame(0, 0);
					X(1) = rawFrame(0, 1);
					X(2) = rawFrame(0, 2);

					Y(0) = rawFrame(1, 0);
					Y(1) = rawFrame(1, 1);
					Y(2) = rawFrame(1, 2);
					double Xdif = nnX.dot(X) / (nnX.norm()*X.norm());
					double Ydif = nnY.dot(Y) / (nnY.norm()*Y.norm());
					cout << "X angle Dif: " << acos(Xdif) << std::endl;
					cout << "Y angle Dif: " << acos(Ydif) << std::endl;

				}
			}
			else {
				rawFrame = dataInstance.getNDITransMat();
				vis.neuralNetworkVisualizer(viewer, rawFrame, "rawFrame");
				nnFrame = getMatDif(dataInstance.getNDITransMat(), stringToTransMat(frameString));
				vis.neuralNetworkVisualizer(viewer, nnFrame, "nnFrame");
			}
			
			sendMessage = ""; // Reset the message
			if (end) { break; }
			ndiClient.sendMessage("1");
		}
		
	}
	readInput.join();
	optoClient.sendMessage("stop");
}

/*
Reads, parses, and writes the optotrak messages into the data container
*/
void readOpto(NeuralDataContainer& currentData, std::string optoMessage) {
	std::vector<std::string> holder;
	boost::split(holder, optoMessage, boost::is_any_of(","), boost::token_compress_on);
	currentData.setOptoTrans(stod(holder.at(0)), stod(holder.at(1)), stod(holder.at(2)));
	double optoRot[3][3] = { { stod(holder.at(3)), stod(holder.at(4)), stod(holder.at(5)) },
	{ stod(holder.at(6)), stod(holder.at(7)), stod(holder.at(8)) },
	{ stod(holder.at(9)), stod(holder.at(10)), stod(holder.at(11)) } };
	currentData.setOptoRot(optoRot);
}

/*
Reads, parses, and writes the kuka messages into the data container
*/
void readKuka(NeuralDataContainer& currentData, std::string kukaMessage, long long kukaTs) {
	if (kukaMessage != "") {
		std::vector<std::string> holder;
		boost::split(holder, kukaMessage, boost::is_any_of(","), boost::token_compress_on);
		currentData.setKukaTrans(stod(holder.at(0)), stod(holder.at(1)), stod(holder.at(2)));
		double kukaRot[] = { stod(holder.at(3)), stod(holder.at(4)), stod(holder.at(5)) };
		currentData.setKukaRot(kukaRot);
		currentData.setKukaTs(kukaTs);
	}
	else { // There is no new kuka data
		if (nnData.size() != 0) {
			NeuralDataContainer prevData = nnData.at(nnData.size() - 1);
			currentData.copyKukaData(prevData, kukaTs); // This will fill the time since last value
		}
	}
}

/*
Reads, parses, and writes the ndi messages into the data container
*/
void readNdi(NeuralDataContainer& currentData, std::string ndiMessage) {
	long long ndiTs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	
	std::vector<std::string> holder;
	boost::split(holder, ndiMessage, boost::is_any_of(","), boost::token_compress_on);
	currentData.setNdiTrans(stod(holder.at(0)), stod(holder.at(1)), stod(holder.at(2)));

	double ndiRot[3][3] = { { stod(holder.at(3)), stod(holder.at(4)), stod(holder.at(5)) },
	{ stod(holder.at(6)), stod(holder.at(7)), stod(holder.at(8)) },
	{ stod(holder.at(9)), stod(holder.at(10)), stod(holder.at(11)) } };
	currentData.setNDIRot(ndiRot);
	currentData.ndiQuality = stod(holder.at(12));
	currentData.setNdiTs(ndiTs);
}

/*
Writes opto, kuka, and ndi data to a csv file with column labels
*/
void writeToFile(std::string fileName, double originalTrans[3], double originalRot[3][3]) {
	ofstream writer, writerAvg;
	vector<string> avgHolder;
	writer.open("C:/Users/cjl/Desktop/NN Repo/TestResults/" + fileName + ".csv");
	writer << "Dif_X,Dif_Y,Dif_Z,Dif_X1,Dif_X2,Dif_X3,Dif_Y1,Dif_Y2,Dif_Y3,Dif_Z1,Dif_Z2,Dif_Z3,";
	writer << "NDI_X,NDI_Y,NDI_Z,NDI_Rot_X1,NDI_Rot_X2,NDI_Rot_X3,NDI_Rot_Y1,NDI_Rot_Y2,NDI_Rot_Y3,NDI_Rot_Z1,NDI_Rot_Z2,NDI_Rot_Z3,NDI_Quality,";
	writer << "Kuka_X,Kuka_Y,Kuka_Z,Kuka_A,Kuka_B,Kuka_C,Kuka_TSL" << std::endl;

	double ndiTransDif[3], ndiRotDif[3][3];
	double curDifY, prevDifY = 0;
	int numSimilar = 0;
	string currentData = "";
	for (int i = 0; i < nnData.size(); i++) {
		NeuralDataContainer current = nnData.at(i);
		if (current.getKukaTSL() == 0) {
			current.setNDIDif(originalTrans, originalRot);
			current.getNDIDif(ndiTransDif, ndiRotDif);
			
			currentData = dataToSend(ndiTransDif, ndiRotDif, current);
			writer << currentData << std::endl;
			avgHolder.push_back(currentData); // Add to data holder for later calculations
		}
	}
	writer.close();
	
	// The below section of code averages data that contains the same result and linearizes it to the next 
	// different point.
	writerAvg.open("C:/Users/cjl/Desktop/NN Repo/TestResults/" + fileName + "Avg.csv");
	writerAvg << "Dif_X,Dif_Y,Dif_Z,Dif_X1,Dif_X2,Dif_X3,Dif_Y1,Dif_Y2,Dif_Y3,Dif_Z1,Dif_Z2,Dif_Z3,";
	writerAvg << "NDI_X,NDI_Y,NDI_Z,NDI_Rot_X1,NDI_Rot_X2,NDI_Rot_X3,NDI_Rot_Y1,NDI_Rot_Y2,NDI_Rot_Y3,NDI_Rot_Z1,NDI_Rot_Z2,NDI_Rot_Z3,NDI_Quality,";
	writerAvg << "Kuka_X,Kuka_Y,Kuka_Z,Kuka_A,Kuka_B,Kuka_C,Kuka_TSL" << std::endl;
	string curString;
	vector<vector<string>> sameNums;
	std::vector<std::string> holder;
	double cur, prev;
	curString = avgHolder.at(0);
	
	boost::split(holder, curString, boost::is_any_of(","), boost::token_compress_on);
	prev = stod(holder.at(1)); //get the Avg Y value
	writerAvg << combineString(holder) << endl;
	for (int i = 1; i < avgHolder.size(); i++) {
		curString = avgHolder.at(i);
		boost::split(holder, curString, boost::is_any_of(","), boost::token_compress_on);
		cur = stod(holder.at(1)); 

		while ((cur == prev) && (i < (avgHolder.size() - 1))) { // Get all samples with same numbers
			sameNums.push_back(holder);
			i++;
			curString = avgHolder.at(i);
			boost::split(holder, curString, boost::is_any_of(","), boost::token_compress_on);
			cur = stod(holder.at(1));
		}

		double step = (cur - prev) / (sameNums.size() + 1);

		//Go through and upadte all same values with the calculated step size
		for (int j = 0; j < sameNums.size(); j++) {
			double newVal = stod(sameNums.at(j).at(1)) + (step*(j+1));
			sameNums.at(j).at(1) = to_string(newVal);
			writerAvg << combineString(sameNums.at(j)) << endl; // Write changes to file to file
		}
		sameNums.clear();

		//Finally write the next new value
		boost::split(holder, curString, boost::is_any_of(","), boost::token_compress_on);
		prev = stod(holder.at(1)); //get the Avg Y value
		writerAvg << combineString(holder) << endl;
	}
	writer.close();
}

/*
Acts as an optotrak stream
*/
void sendOptoMessage(std::string message) {
	while (optoServ.getMessage() != "start") {}
	while (true) {
		optoServ.sendMessage(message);
		std::this_thread::sleep_for(std::chrono::milliseconds(2));
	}
}

/*
Compares the Difference in optotrak data to difference in ndi data
*/
void compOptoNDI(int numSamples) {


	std::string optoMessage;
	std::string ndiMessage;

	int curSample = 0;
	StopWatch newWatch;
	optoClient.sendMessage("start");
	ndiClient.sendMessage("1");
	while (curSample < numSamples) {
		optoMessage = optoClient.getMessage();
		if (optoMessage != "") {
			ndiMessage = ndiClient.getMessage();
			NeuralDataContainer dataInstance;
			readOpto(dataInstance, optoMessage);
			readNdi(dataInstance, ndiMessage);
			nnData.push_back(dataInstance);
			curSample++;
		}
		//newWatch.stampedLap("Lap:");
	}
	optoClient.sendMessage("stop");
}

void writeDifToFile(std::string fileName) {
	ofstream writer;
	writer.open("C:\\Users\\cjl\\Desktop\\NN Repo\\TestResults\\" + fileName + ".csv");
	writer << "Opto_Dif_X,NDI_Dif_X,Opto_Dif_Y,NDI_Dif_Y,Opto_Dif_Z,NDI_Dif_Z,";
	writer << "Opto_Dif_X1,NDI_Dif_X1,Opto_Dif_X2,NDI_Dif_X2,Opto_Dif_X3,NDI_Dif_X3,";
	writer << "Opto_Dif_Y1,NDI_Dif_Y1,Opto_Dif_Y2,NDI_Dif_Y2,Opto_Dif_Y3,NDI_Dif_Y3,";
	writer << "Opto_Dif_Z1,NDI_Dif_Z1,Opto_Dif_Z2,NDI_Dif_Z2,Opto_Dif_Z3,NDI_Dif_Z3" << std::endl;

	double optoRot[3][3], ndiRot[3][3], optoTrans[3], ndiTrans[3];
	double ndiTransDif[3], ndiRotDif[3][3], optoTransDif[3], optoRotDif[3][3];
	for (int i = 1; i < nnData.size(); i++) {
		NeuralDataContainer prev = nnData.at(i - 1);
		NeuralDataContainer current = nnData.at(i);
		prev.getNDIRot(ndiRot);
		prev.getNDITrans(ndiTrans);
		prev.getOptoRot(optoRot);
		prev.getOptoTrans(optoTrans);
		current.setNDIDif(ndiTrans, ndiRot);
		current.setOptoDif(optoTrans, optoRot);
		current.getNDIDif(ndiTransDif, ndiRotDif);
		current.getOptoDif(optoTransDif, optoRotDif);
		writer << optoTransDif[0] << "," << ndiTransDif[0] << "," << optoTransDif[1] << "," << ndiTransDif[1] << "," << optoTransDif[2] << "," << ndiTransDif[2] << ",";
		writer << optoRotDif[0][0] << "," << ndiRotDif[0][0] << ",";
		writer << optoRotDif[0][1] << "," << ndiRotDif[0][1] << ",";
		writer << optoRotDif[0][2] << "," << ndiRotDif[0][2] << ",";
		writer << optoRotDif[1][0] << "," << ndiRotDif[1][0] << ",";
		writer << optoRotDif[1][1] << "," << ndiRotDif[1][1] << ",";
		writer << optoRotDif[1][2] << "," << ndiRotDif[1][2] << ",";
		writer << optoRotDif[2][0] << "," << ndiRotDif[2][0] << ",";
		writer << optoRotDif[2][1] << "," << ndiRotDif[2][1] << ",";
		writer << optoRotDif[2][2] << "," << ndiRotDif[2][2] << endl;
	}
	writer.close();
}