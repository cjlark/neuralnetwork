#include "nnHelper.h"
#include <string>

BoostSoc kukaClient, optoClient, ndiClient, UIserver;
bool readFlag = true;
long long kukaOffset = 11088735, optoOffset = 0;
// Updates the status of the connections of each of the streams
void sendStatus(int optoStatus, int ndiStatus, int kukaStatus);
// Periodically monitors the status of each stream (every second)
void monitorStatus(int optoStatus, int ndiStatus, int kukaStatus);
// Reads the incoming messages from each stream and outputs to a provided file path
void readMessage(std::string fileName, std::string name, int sleepTime, double numSamples);

int main()
{
	BoostSoc optoServ, ndiServ, kukaServ;
	//kukaServ.clientConnect(2000,"127.0.0.1", false, -1);
	optoServ.clientConnect(2003, "127.0.0.1", false, -1);
	//ndiServ.clientConnect(2004, "127.0.0.1", false, -1);
	
	// Connect to the UI
	UIserver.serverConnect(2001, false);
	while (!UIserver.getConnected()) { //Wait until the UI is connected
		std::cout << "Not Connected to UI!\n";
		std::cout << "Attempting...\n";
		Sleep(1000);
	}

	std::string UIMessage = UIserver.getMessage();
	std::string fileName;
	double numSamples, sampleRate;

	while (UIMessage == "") { // Recieve messages until you get the message from the UI
		UIMessage = UIserver.getMessage();
	}

	parseUIMessage(UIMessage, fileName, numSamples, sampleRate);

	std::cout << "File name specified as: " << fileName << std::endl;

	int optoStatus = 0, ndiStatus = 0, kukaStatus = 0;
	sendStatus(optoStatus, ndiStatus, kukaStatus); //send intialization message

	// Attempt connection to the three streaming devices
	connectSocket(kukaClient, 2000, "Kuka", kukaStatus);
	sendStatus(optoStatus, ndiStatus, kukaStatus);
	connectSocket(optoClient, 2003, "optoClient", optoStatus);
	sendStatus(optoStatus, ndiStatus, kukaStatus);
	connectSocket(ndiClient, 2004, "ndiClient", ndiStatus);
	sendStatus(optoStatus, ndiStatus, kukaStatus);
	
	std::thread sendStatus(monitorStatus, optoStatus, ndiStatus, kukaStatus);
	Sleep(1000);
	
	std::cout << "Collecting Data...\n";

	readMessage(fileName, "test1", sampleRate, numSamples);
	
	std::cout << numSamples << " samples collected!\n";
	std::cout << "Cleaning up data...\n";
	cleanData(fileName, numSamples, kukaOffset, optoOffset);
	std::cout << "Data cleaned!\n";

	system("pause");
}

void sendStatus(int optoStatus, int ndiStatus, int kukaStatus) {
		UIserver.sendMessage(std::to_string(optoClient.getConnected()) + "," + std::to_string(ndiClient.getConnected()) + "," + std::to_string(kukaClient.getConnected()));
	
}

void monitorStatus(int optoStatus, int ndiStatus, int kukaStatus) {
	// Periodically checks the status of the connections and sends info to UI
	while (true) {
		optoStatus = optoClient.getConnected() ? 1 : 0;
		ndiStatus = ndiClient.getConnected() ? 1 : 0;
		kukaStatus = kukaClient.getConnected() ? 1 : 0;
		sendStatus(optoStatus, ndiStatus, kukaStatus);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

void readMessage(std::string fileName, std::string name, int sleepTime, double numSamples) {

	ofstream NDIwriter, KUKAwriter, OPTOwriter;
	NDIwriter.open("C:/Users/cjl/Desktop/Neural Network Coms/TestResults/"+ fileName + "_ndi.csv");
	KUKAwriter.open("C:/Users/cjl/Desktop/Neural Network Coms/TestResults/" + fileName + "_kuka.csv");
	OPTOwriter.open("C:/Users/cjl/Desktop/Neural Network Coms/TestResults/" + fileName + "_opto.csv");
	int count = 0;
	ndiClient.sendMessage("1");
	//kukaClient.sendMessage("1");
	optoClient.sendMessage("1," + to_string(numSamples));
	StopWatch timer;
	while (count < numSamples) {
		//OPTOwriter <<  optoClient.getMessage() << ",\n";
		OPTOwriter << duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count() << ",\n";
		//ndiClient.sendMessage("1");
		
		NDIwriter << ndiClient.getMessage() << ",\n";
		//kukaClient.sendMessage("1");

		KUKAwriter << kukaClient.getMessage() << ",\n";

		count++;
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
		//cout << count << endl;
	}
	ndiClient.sendMessage("0");
	NDIwriter.close();
	OPTOwriter.close();
	KUKAwriter.close();
	readFlag = false;
}