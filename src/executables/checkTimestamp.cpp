#include <iostream>
#include <chrono>
#include <string>
#include <fstream>
#include "BoostSoc.h"
#include "StopWatch.h"

std::string incomingStamp = "1530817430871";
BoostSoc server;

// Converts incoming timestamp to a long long and compares it to the current system time
long long calcStampDif(long long systemTime, std::string currentStamp) {
	long long compStamp = stoll(currentStamp.substr(0, 13));
	long long difference = compStamp < systemTime ? (systemTime - compStamp) : (compStamp - systemTime);
	return difference;
}

// If input is 1 then print the stamp to the screen
milliseconds getStamp(int x) {
	while (true) {
		string tempStamp = server.getMessage();
		if (tempStamp != "") {
			incomingStamp = tempStamp;
		}
	
		if (x == 1) {
			std::cout << incomingStamp << std::endl;
		}
	}
}

int main() {

	int port;
	std::cout << "Please enter the port you would like to connect to!\n";
	std::cin >> port;
	
	server.serverConnect(port, false);
	while (!server.getConnected()) {
		cout << "Attempting connection!\n";
		Sleep(1000);
	}
	cout << "Connected!\n";

	Sleep(2000);
	if (server.getConnected()) {
		server.sendMessage("start"); // Sends message as 2 to allow the receiving program to know only to send the timeStamp;
	}
	cout << "Sent Request\n";
	//std::thread getMessages(getStamp, 1); // Runs a thread to continually update the string in the background

	string name;
	//cout << "Enter file name: \n";
	//cin >> name;
	ofstream writer;
	writer.open("C:/Users/cjl/Desktop/Neural Network Coms/TestResults/Timestamp Tests/" + name +".csv");
	int x = 0;
	long long prevDif = 0;
	while (x < 250) {
		cout << server.getMessage() << endl;
			//chrono::milliseconds systemTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
			//std::string curStamp = incomingStamp;
			//long long newDif = calcStampDif(systemTime.count(), curStamp);
			//writer << newDif << ", "<< (newDif-prevDif) <<std::endl;
			//prevDif = newDif;
			x++;
	}
	
	server.sendMessage("stop");
	//getMessages.join();
	cout << "DONE!\n";
	system("pause");
	return 0;
}